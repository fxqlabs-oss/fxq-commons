from enum import Enum


class Country(Enum):
    AFGHANISTAN = "AF", "AFG",
    ALBANIA = "AL", "ALB",
    ALGERIA = "DZ", "DZA",
    AMERICAN_SAMOA = "AS", "ASM",
    ANDORRA = "AD", "AND",
    ANGOLA = "AO", "AGO",
    ANGUILLA = "AI", "AIA",
    ANTARCTICA = "AQ", "ATA",
    ANTIGUA_AND_BARBUDA = "AG", "ATG",
    ARGENTINA = "AR", "ARG",
    ARMENIA = "AM", "ARM",
    ARUBA = "AW", "ABW",
    AUSTRALIA = "AU", "AUS",
    AUSTRIA = "AT", "AUT",
    AZERBAIJAN = "AZ", "AZE",
    BAHAMAS = "BS", "BHS",
    BAHRAIN = "BH", "BHR",
    BANGLADESH = "BD", "BGD",
    BARBADOS = "BB", "BRB",
    BELARUS = "BY", "BLR",
    BELGIUM = "BE", "BEL",
    BELIZE = "BZ", "BLZ",
    BENIN = "BJ", "BEN",
    BERMUDA = "BM", "BMU",
    BHUTAN = "BT", "BTN",
    BOLIVIA = "BO", "BOL",
    BONAIRE_SINT_EUSTATIUS_AND_SABA = "BQ", "BES",
    BOSNIA_AND_HERZEGOVINA = "BA", "BIH",
    BOTSWANA = "BW", "BWA",
    BOUVET_ISLAND = "BV", "BVT",
    BRAZIL = "BR", "BRA",
    BRITISH_INDIAN_OCEAN_TERRITORY = "IO", "IOT",
    BRUNEI_DARUSSALAM = "BN", "BRN",
    BULGARIA = "BG", "BGR",
    BURKINA_FASO = "BF", "BFA",
    BURUNDI = "BI", "BDI",
    CABO_VERDE = "CV", "CPV",
    CAMBODIA = "KH", "KHM",
    CAMEROON = "CM", "CMR",
    CANADA = "CA", "CAN",
    CAYMAN_ISLANDS = "KY", "CYM",
    CENTRAL_AFRICAN_REPUBLIC = "CF", "CAF",
    CHAD = "TD", "TCD",
    CHILE = "CL", "CHL",
    CHINA = "CN", "CHN",
    CHRISTMAS_ISLAND = "CX", "CXR",
    COCOS_ISLANDS = "CC", "CCK",
    COLOMBIA = "CO", "COL",
    COMOROS = "KM", "COM",
    CONGO_DR = "CD", "COD",
    CONGO = "CG", "COG",
    COOK_ISLANDS = "CK", "COK",
    COSTA_RICA = "CR", "CRI",
    CROATIA = "HR", "HRV",
    CUBA = "CU", "CUB",
    CURACAO = "CW", "CUW",
    CYPRUS = "CY", "CYP",
    CZECHIA = "CZ", "CZE",
    COTE_D_IVOIRE = "CI", "CIV",
    DENMARK = "DK", "DNK",
    DJIBOUTI = "DJ", "DJI",
    DOMINICA = "DM", "DMA",
    DOMINICAN_REPUBLIC = "DO", "DOM",
    ECUADOR = "EC", "ECU",
    EGYPT = "EG", "EGY",
    EL_SALVADOR = "SV", "SLV",
    EQUATORIAL_GUINEA = "GQ", "GNQ",
    ERITREA = "ER", "ERI",
    ESTONIA = "EE", "EST",
    ESWATINI = "SZ", "SWZ",
    ETHIOPIA = "ET", "ETH",
    FALKLAND_ISLANDS = "FK", "FLK",
    FAROE_ISLANDS = "FO", "FRO",
    FIJI = "FJ", "FJI",
    FINLAND = "FI", "FIN",
    FRANCE = "FR", "FRA",
    FRENCH_GUIANA = "GF", "GUF",
    FRENCH_POLYNESIA = "PF", "PYF",
    FRENCH_SOUTHERN_TERRITORIES = "TF", "ATF",
    GABON = "GA", "GAB",
    GAMBIA = "GM", "GMB",
    GEORGIA = "GE", "GEO",
    GERMANY = "DE", "DEU",
    GHANA = "GH", "GHA",
    GIBRALTAR = "GI", "GIB",
    GREECE = "GR", "GRC",
    GREENLAND = "GL", "GRL",
    GRENADA = "GD", "GRD",
    GUADELOUPE = "GP", "GLP",
    GUAM = "GU", "GUM",
    GUATEMALA = "GT", "GTM",
    GUERNSEY = "GG", "GGY",
    GUINEA = "GN", "GIN",
    GUINEA_BISSAU = "GW", "GNB",
    GUYANA = "GY", "GUY",
    HAITI = "HT", "HTI",
    HEARD_ISLAND_AND_MC_DONALD_ISLANDS = "HM", "HMD",
    HOLY_SEE = "VA", "VAT",
    HONDURAS = "HN", "HND",
    HONG_KONG = "HK", "HKG",
    HUNGARY = "HU", "HUN",
    ICELAND = "IS", "ISL",
    INDIA = "IN", "IND",
    INDONESIA = "ID", "IDN",
    IRAN = "IR", "IRN",
    IRAQ = "IQ", "IRQ",
    IRELAND = "IE", "IRL",
    ISLE_OF_MAN = "IM", "IMN",
    ISRAEL = "IL", "ISR",
    ITALY = "IT", "ITA",
    JAMAICA = "JM", "JAM",
    JAPAN = "JP", "JPN",
    JERSEY = "JE", "JEY",
    JORDAN = "JO", "JOR",
    KAZAKHSTAN = "KZ", "KAZ",
    KENYA = "KE", "KEN",
    KIRIBATI = "KI", "KIR",
    KOREA_NORTH = "KP", "PRK",
    KOREA_SOUTH = "KR", "KOR",
    KUWAIT = "KW", "KWT",
    KYRGYZSTAN = "KG", "KGZ",
    LAO_PEOPLE_S_DEMOCRATIC_REPUBLIC = "LA", "LAO",
    LATVIA = "LV", "LVA",
    LEBANON = "LB", "LBN",
    LESOTHO = "LS", "LSO",
    LIBERIA = "LR", "LBR",
    LIBYA = "LY", "LBY",
    LIECHTENSTEIN = "LI", "LIE",
    LITHUANIA = "LT", "LTU",
    LUXEMBOURG = "LU", "LUX",
    MACAO = "MO", "MAC",
    MADAGASCAR = "MG", "MDG",
    MALAWI = "MW", "MWI",
    MALAYSIA = "MY", "MYS",
    MALDIVES = "MV", "MDV",
    MALI = "ML", "MLI",
    MALTA = "MT", "MLT",
    MARSHALL_ISLANDS = "MH", "MHL",
    MARTINIQUE = "MQ", "MTQ",
    MAURITANIA = "MR", "MRT",
    MAURITIUS = "MU", "MUS",
    MAYOTTE = "YT", "MYT",
    MEXICO = "MX", "MEX",
    MICRONESIA = "FM", "FSM",
    MOLDOVA = "MD", "MDA",
    MONACO = "MC", "MCO",
    MONGOLIA = "MN", "MNG",
    MONTENEGRO = "ME", "MNE",
    MONTSERRAT = "MS", "MSR",
    MOROCCO = "MA", "MAR",
    MOZAMBIQUE = "MZ", "MOZ",
    MYANMAR = "MM", "MMR",
    NAMIBIA = "NA", "NAM",
    NAURU = "NR", "NRU",
    NEPAL = "NP", "NPL",
    NETHERLANDS = "NL", "NLD",
    NEW_CALEDONIA = "NC", "NCL",
    NEW_ZEALAND = "NZ", "NZL",
    NICARAGUA = "NI", "NIC",
    NIGER = "NE", "NER",
    NIGERIA = "NG", "NGA",
    NIUE = "NU", "NIU",
    NORFOLK_ISLAND = "NF", "NFK",
    NORTHERN_MARIANA_ISLANDS = "MP", "MNP",
    NORWAY = "NO", "NOR",
    OMAN = "OM", "OMN",
    PAKISTAN = "PK", "PAK",
    PALAU = "PW", "PLW",
    PALESTINE_STATE_OF = "PS", "PSE",
    PANAMA = "PA", "PAN",
    PAPUA_NEW_GUINEA = "PG", "PNG",
    PARAGUAY = "PY", "PRY",
    PERU = "PE", "PER",
    PHILIPPINES = "PH", "PHL",
    PITCAIRN = "PN", "PCN",
    POLAND = "PL", "POL",
    PORTUGAL = "PT", "PRT",
    PUERTO_RICO = "PR", "PRI",
    QATAR = "QA", "QAT",
    REPUBLIC_OF_NORTH_MACEDONIA = "MK", "MKD",
    ROMANIA = "RO", "ROU",
    RUSSIAN_FEDERATION = "RU", "RUS",
    RWANDA = "RW", "RWA",
    REUNION = "RE", "REU",
    SAINT_BARTHELEMY = "BL", "BLM",
    SAINT_HELENA_ASCENSION_AND_TRISTAN_DA_CUNHA = "SH", "SHN",
    SAINT_KITTS_AND_NEVIS = "KN", "KNA",
    SAINT_LUCIA = "LC", "LCA",
    SAINT_MARTIN = "MF", "MAF",
    SAINT_PIERRE_AND_MIQUELON = "PM", "SPM",
    SAINT_VINCENT_AND_THE_GRENADINES = "VC", "VCT",
    SAMOA = "WS", "WSM",
    SAN_MARINO = "SM", "SMR",
    SAO_TOME_AND_PRINCIPE = "ST", "STP",
    SAUDI_ARABIA = "SA", "SAU",
    SENEGAL = "SN", "SEN",
    SERBIA = "RS", "SRB",
    SEYCHELLES = "SC", "SYC",
    SIERRA_LEONE = "SL", "SLE",
    SINGAPORE = "SG", "SGP",
    SINT_MAARTEN = "SX", "SXM",
    SLOVAKIA = "SK", "SVK",
    SLOVENIA = "SI", "SVN",
    SOLOMON_ISLANDS = "SB", "SLB",
    SOMALIA = "SO", "SOM",
    SOUTH_AFRICA = "ZA", "ZAF",
    SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS = "GS", "SGS",
    SOUTH_SUDAN = "SS", "SSD",
    SPAIN = "ES", "ESP",
    SRI_LANKA = "LK", "LKA",
    SUDAN = "SD", "SDN",
    SURINAME = "SR", "SUR",
    SVALBARD_AND_JAN_MAYEN = "SJ", "SJM",
    SWEDEN = "SE", "SWE",
    SWITZERLAND = "CH", "CHE",
    SYRIAN_ARAB_REPUBLIC = "SY", "SYR",
    TAIWAN = "TW", "TWN",
    TAJIKISTAN = "TJ", "TJK",
    TANZANIA_UNITED_REPUBLIC_OF = "TZ", "TZA",
    THAILAND = "TH", "THA",
    TIMOR_LESTE = "TL", "TLS",
    TOGO = "TG", "TGO",
    TOKELAU = "TK", "TKL",
    TONGA = "TO", "TON",
    TRINIDAD_AND_TOBAGO = "TT", "TTO",
    TUNISIA = "TN", "TUN",
    TURKEY = "TR", "TUR",
    TURKMENISTAN = "TM", "TKM",
    TURKS_AND_CAICOS_ISLANDS = "TC", "TCA",
    TUVALU = "TV", "TUV",
    UGANDA = "UG", "UGA",
    UKRAINE = "UA", "UKR",
    UNITED_ARAB_EMIRATES = "AE", "ARE",
    UNITED_KINGDOM = "GB", "GBR",
    UNITED_STATES_MINOR_OUTLYING_ISLANDS = "UM", "UMI",
    UNITED_STATES_OF_AMERICA = "US", "USA",
    URUGUAY = "UY", "URY",
    UZBEKISTAN = "UZ", "UZB",
    VANUATU = "VU", "VUT",
    VENEZUELA = "VE", "VEN",
    VIET_NAM = "VN", "VNM",
    VIRGIN_ISLANDS_BRIT = "VG", "VGB",
    VIRGIN_ISLANDS_US = "VI", "VIR",
    WALLIS_AND_FUTUNA = "WF", "WLF",
    WESTERN_SAHARA = "EH", "ESH",
    YEMEN = "YE", "YEM",
    ZAMBIA = "ZM", "ZMB",
    ZIMBABWE = "ZW", "ZWE",
    ALAND_ISLANDS = "AX", "ALA"

    @property
    def alpha2(self):
        """Alpha 2 Code of the Country"""
        return self.value[0]

    @property
    def alpha3(self):
        """Alpha 3 Code of the Country"""
        return self.value[1]

    def get_alpha2(self):
        """
        Get the Alpha2 Code of the Enum

        :return: Returns the alpha2 code as a string
        """
        return self.alpha2

    def get_alpha3(self):
        """
        Get the Alpha3 Code of the Enum

        :return: Returns the alpha3 code as a string
        """
        return self.alpha3

    @staticmethod
    def value_of(alpha_code: str):
        """
        Parses the provided alpha_code into the Enum, takes either the Alpha2 or Alpha3 code

        i.e.

        "AFG" -> FxqCountry.AFGHANISTAN

        :param alpha_code: Alpha2 or Alpha3 code for the country
        :return: Returns the FxqCountry Enum
        """
        if len(alpha_code) not in [2, 3]:
            raise Exception(
                f"Failed to find the value of '{alpha_code}' Unsupported Alpha Code Length")

        try:
            if len(alpha_code) == 2:
                return [e for e in Country.__members__.values() if e.alpha2 == alpha_code][0]
            elif len(alpha_code) == 3:
                return [e for e in Country.__members__.values() if e.alpha3 == alpha_code][0]
        except IndexError as ie:
            raise Exception(
                f"Failed to match the alpha code of '{alpha_code}'") from None
