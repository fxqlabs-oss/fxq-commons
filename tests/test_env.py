import os
from pathlib import Path
from unittest import TestCase
from unittest.mock import MagicMock, patch

from fxq.env import Environment

mock_open = MagicMock()

test_folder_abs_path = Path(os.path.realpath(__file__)).parent
test_folder_resources_path = Path(test_folder_abs_path, "resources")

app_yml = """
app:
  name: test-app-name
  version: 1.0.0
"""

app_test_yml = """
app:
  name: test-app-name
  version: 1.0.1
  database: test_db
"""

mock_open = MagicMock()


def read_app_config_yml(path):
    file_handle = MagicMock()
    str_resp = {
        "app.yml": app_test_yml,
        "app.test.yml": app_test_yml
    }[path]
    file_handle.read.return_value = str_resp
    return file_handle


class TestEnvironment(TestCase):

    @patch("builtins.open", mock_open)
    def setUp(self) -> None:
        mock_open.side_effect = read_app_config_yml
        self.env = Environment()

    def test_reads_config_from_app_yml(self):
        mock_open.assert_called_with("app.yml")

    def test_get_property_without_profile_loads_default(self):
        self.assertEqual(self.env.get_property("app.name"), "test-app-name")


class TestEnvironmentWithTestProfile(TestCase):

    @patch("builtins.open", mock_open)
    def setUp(self) -> None:
        mock_open.side_effect = read_app_config_yml
        self.env = Environment(active_profiles=["test"])

    def test_reads_config_from_app_yml(self):
        mock_open.mock_calls[0].assert_called_with("app.yml")

    def test_reads_config_from_app_test_yml(self):
        mock_open.mock_calls[1].assert_called_with("app.test.yml")

    def test_get_property_with_profile_loads_override(self):
        self.assertEqual("1.0.1", self.env.get_property("app.version"))

    def test_get_property_with_profile_loads_additional_item(self):
        self.assertEqual("test_db", self.env.get_property("app.database"))
