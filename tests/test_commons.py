from unittest import TestCase

from fxq.commons import dict_merge


class TestCommons(TestCase):
    def test_dict_merge_merges_two_simple_dictionaries(self):
        d1 = {
            "a": "_A"
        }
        d2 = {
            "b": "_B"
        }
        dict_merge(d1, d2)

        self.assertEqual(d1["a"], "_A")
        self.assertEqual(d1["b"], "_B")

    def test_dict_merge_merges_a_deep_dictionary(self):
        d1 = {
            "a": "_A"
        }
        d2 = {
            "b": "_B",
            "c": {
                "ca": "_CA"
            }
        }
        dict_merge(d1, d2)

        self.assertEqual(d1["c"]["ca"], "_CA")

    def test_dict_merge_merges_a_deep_dictionary_where_key_already_exists(self):
        d1 = {
            "a": "_A",
            "b": {
                "ba": "_BA",
            }
        }
        d2 = {
            "b": {
                "bb": "_BB"
            }
        }
        dict_merge(d1, d2)

        self.assertEqual(d1["b"]["ba"], "_BA")
        self.assertEqual(d1["b"]["bb"], "_BB")

    def test_dict_merge_merges_a_deep_dictionary_replacing_values(self):
        d1 = {
            "a": "_A",
            "b": {
                "ba": "_BA",
            }
        }
        d2 = {
            "b": {
                "ba": "__BA"
            }
        }
        self.assertEqual(d1["b"]["ba"], "_BA")
        dict_merge(d1, d2)
        self.assertEqual(d1["b"]["ba"], "__BA")
