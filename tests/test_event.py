from unittest import TestCase

from fxq.model.event import Impact, Type


class TestEventImpact(TestCase):
    def test_get_id(self):
        self.assertEqual(0, Impact.NONE.id)
        self.assertEqual(2, Impact.MEDIUM.id)

    def test_get_name(self):
        self.assertEqual("None", Impact.NONE.name)
        self.assertEqual("Medium", Impact.MEDIUM.name)

    def test_parse_id(self):
        self.assertEqual(Impact.NONE, Impact.value_of(0))
        self.assertEqual(Impact.LOW, Impact.value_of(1))

    def test_parse_name(self):
        self.assertEqual(Impact.NONE, Impact.value_of("None"))
        self.assertEqual(Impact.LOW, Impact.value_of("Low"))
        self.assertEqual(Impact.LOW, Impact.value_of("low"))

class TestEventType(TestCase):
    def test_get_name(self):
        self.assertEqual("Liquidity & Balance", Type.LIQUIDITY_AND_BALANCE.name)
        self.assertEqual("Liquidity & Balance", Type.LIQUIDITY_AND_BALANCE.get_name())

    def test_get_id(self):
        self.assertEqual(1, Type.CENTRAL_BANKS.id)
        self.assertEqual(2, Type.CONFIDENCE_INDICES.get_id())

    def test_parse_name(self):
        self.assertEqual(Type.LIQUIDITY_AND_BALANCE, Type.value_of(10))
        self.assertEqual(Type.LIQUIDITY_AND_BALANCE, Type.value_of("Liquidity & Balance"))
