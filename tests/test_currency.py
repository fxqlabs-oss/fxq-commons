from unittest import TestCase

from fxq.model.currency import Currency


class TestCurrency(TestCase):
    def test_get_code(self):
        self.assertEqual("GBP", Currency.POUND_STERLING.code)
        self.assertEqual("GBP", Currency.POUND_STERLING.get_code())

    def test_get_symbol(self):
        self.assertEqual("£", Currency.POUND_STERLING.symbol)
        self.assertEqual("£", Currency.POUND_STERLING.get_symbol())

    def test_value_of_with_code(self):
        self.assertEqual(Currency.POUND_STERLING, Currency.value_of("GBP"))
