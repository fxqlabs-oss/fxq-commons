from typing import List
from unittest import TestCase

from fxq.commons.marshalling import DictMarshal


class Metadata:
    def __init__(self, startDate=None, endDate=None):
        self.startDate = startDate
        self.endDate = endDate


class Todo:
    types = {"metadata": Metadata}

    def __init__(self, userId=None, id=None, title=None, body=None, metadata=None):
        self.userId = userId
        self.id = id
        self.title = title
        self.body = body
        self.metadata = metadata


class TestDictMarshal(TestCase):

    def setUp(self) -> None:
        self.dm = DictMarshal()

    def test_to_dict_converts_object_to_dictionary(self):
        todo = Todo(10, 1, "TITLE 1", "BODY 1", Metadata("2019-01-01", "2019-01-02"))
        dict_ = self.dm.to_dict(todo)
        self.assertEqual(todo.userId, dict_["userId"])
        self.assertEqual(todo.id, dict_["id"])
        self.assertEqual(todo.title, dict_["title"])
        self.assertEqual(todo.body, dict_["body"])
        self.assertEqual(todo.metadata.startDate, dict_["metadata"]["startDate"])
        self.assertEqual(todo.metadata.endDate, dict_["metadata"]["endDate"])

    def test_from_dict_converts_to_specified_type(self):
        td = {
            "userId": 20,
            "id": 2,
            "title": "TITLE 2",
            "body": "BODY 2",
            "metadata": {
                "startDate": "2019-01-01",
                "endDate": "2019-01-02"
            }
        }
        todo: Todo = self.dm.from_dict(td, Todo)
        self.assertEqual(td["userId"], todo.userId)
        self.assertEqual(td["id"], todo.id)
        self.assertEqual(td["title"], todo.title)
        self.assertEqual(td["body"], todo.body)
        self.assertEqual(td["metadata"]["startDate"], todo.metadata.startDate)
        self.assertEqual(td["metadata"]["endDate"], todo.metadata.endDate)

    def test_from_dict_converts_list_to_specified_type(self):
        tl = [
            {
                "userId": 10,
                "id": 1,
                "title": "TITLE 1",
                "body": "BODY 1",
                "metadata": {
                    "startDate": "2019-01-01",
                    "endDate": "2019-01-02"
                }
            },
            {
                "userId": 20,
                "id": 2,
                "title": "TITLE 2",
                "body": "BODY 2",
                "metadata": {
                    "startDate": "2019-01-01",
                    "endDate": "2019-01-02"
                }
            }
        ]
        todos: List[Todo] = self.dm.from_dict(tl, Todo)
        self.assertEqual(tl[0]["userId"], todos[0].userId)
        self.assertEqual(tl[0]["id"], todos[0].id)
        self.assertEqual(tl[0]["title"], todos[0].title)
        self.assertEqual(tl[0]["body"], todos[0].body)
        self.assertEqual(tl[0]["metadata"]["startDate"], todos[0].metadata.startDate)
        self.assertEqual(tl[0]["metadata"]["endDate"], todos[0].metadata.endDate)

        self.assertEqual(tl[1]["userId"], todos[1].userId)
        self.assertEqual(tl[1]["id"], todos[1].id)
        self.assertEqual(tl[1]["title"], todos[1].title)
        self.assertEqual(tl[1]["body"], todos[1].body)
        self.assertEqual(tl[1]["metadata"]["startDate"], todos[1].metadata.startDate)
        self.assertEqual(tl[1]["metadata"]["endDate"], todos[1].metadata.endDate)
