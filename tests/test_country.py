from unittest import TestCase

from fxq.model.country import Country


class TestCountry(TestCase):
    def test_get_alpha2(self):
        self.assertEqual("GB", Country.UNITED_KINGDOM.alpha2)
        self.assertEqual("GB", Country.UNITED_KINGDOM.get_alpha2())

    def test_get_alpha3(self):
        self.assertEqual("GBR", Country.UNITED_KINGDOM.alpha3)
        self.assertEqual("GBR", Country.UNITED_KINGDOM.get_alpha3())

    def test_parse_alpha2(self):
        self.assertEqual(Country.UNITED_KINGDOM, Country.value_of("GB"))

    def test_parse_alpha3(self):
        self.assertEqual(Country.UNITED_KINGDOM, Country.value_of("GBR"))
