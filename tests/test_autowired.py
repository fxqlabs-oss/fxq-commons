from unittest import TestCase

from fxq.core.beans.factory.annotation import Autowired
from fxq.core.stereotype import Component


@Component
class TestComponentA:

    def __init__(self):
        pass


@Component
class TestComponentB:

    def __init__(self):
        pass


class TestServiceWithFieldInjection:

    def __init__(self):
        self.test_component_a = Autowired(type=TestComponentA)
        self.test_component_b = Autowired(type=TestComponentB)
        self.instance_variable = None
        local_variable = None


class TestServiceWithConstructorInjection:

    @Autowired
    def __init__(self, test_component_a, test_component_b):
        self.test_component_a = test_component_a
        self.test_component_b = test_component_b
        self.instance_variable = None
        local_variable = None


class TestAutowired(TestCase):
    def test_AutowiredCallReturnsCorrectTypeForTestComponentAWhenCalledFromType(self):
        test_component = Autowired(type=TestComponentA)
        self.assertIsInstance(test_component, TestComponentA)

    def test_AutowiredCallReturnsCorrectBeanForTestComponentAWhenCalledFromName(self):
        test_component = Autowired(name="test_component_a")
        self.assertIsInstance(test_component, TestComponentA)

    def test_AutowiredCallReturnsCorrectBeanForTestComponentAWhenCalledFromNameAndType(self):
        test_component = Autowired(name="test_component_a", type=TestComponentA)
        self.assertIsInstance(test_component, TestComponentA)

    def test_AutowiredCallThrowsExceptionWhenNameIsOfUnexpectedType(self):
        with self.assertRaises(Exception):
            test_component = Autowired(name=10)

    def test_ServiceWithAutowiredFieldsContainsExpectedBeans(self):
        test_service = TestServiceWithFieldInjection()
        self.assertIsInstance(test_service.test_component_a, TestComponentA)
        self.assertIsInstance(test_service.test_component_b, TestComponentB)

    def test_ServiceWithAutowiredConstructorContainsExpectedBeans(self):
        test_service = TestServiceWithConstructorInjection()
        self.assertIsInstance(test_service.test_component_a, TestComponentA)
        self.assertIsInstance(test_service.test_component_b, TestComponentB)
