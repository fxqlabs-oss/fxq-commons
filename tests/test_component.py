from unittest import TestCase

from fxq.core.context.ApplicationContext import application_context
from fxq.core.stereotype import Component


@Component
class CmpTestComponentA:
    def __init__(self):
        pass


@Component(name="cmpTestComponentB")
class CmpTestComponentB:
    def __init__(self):
        pass


class TestComponent(TestCase):
    def test_ComponentAExistsInApplicationContext(self):
        self.assertIsInstance(application_context.get_bean(name="cmp_test_component_a"), CmpTestComponentA)

    def test_ComponentBExistsInApplicationContextWithCustomName(self):
        self.assertIsInstance(application_context.get_bean(name="cmpTestComponentB"), CmpTestComponentB)
