# FXQLabs-OSS Commons

This repository is a common codebase shared by many projects in the FXQLabs ecosystem.

It contains collections of classes and functions that provide useful functionality when building
out python apps.

Any re-usable part of an application is abstracted and brought into the commons library for re-use.

### Getting Started
Please see the repository [WIKI](https://bitbucket.org/fxqlabs-oss/fxq-commons/wiki/browse/) for further information on getting started

### Installing

Simply install the python module using 
```bash
pip install fxq-commons
```

## Contributing

Contributions are most welcome to the project, please raise issues first and contribute in response to the issue with a pull request.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/fxquants/fxq-ioc-core/downloads/?tab=tags). 

## Authors

* [Jonathan Turnock](https://bitbucket.org/Jonathan_Turnock/) - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
